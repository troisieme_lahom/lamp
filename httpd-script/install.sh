#!/bin/bash

# Prompt the user to enter a name for the website
read -p "Please enter your name: " WEBSITE
read -s "Please enter database root password" DATABASE_PASSWD

 verify if the website has extention
if [[ "$WEBSITE" != *"."* ]]; then
    echo "Invalid input! Please enter a name with an extension."
		exit 1
fi

SITE=$(echo "$WEBSITE" | cut -d'.' -f1)

SITE_DB=${SITE}db

DB_USER=${SITE}user

USER_PASSWD=${SITE}passwd

echo "$(hostname -I | awk '{print $2}') $WEBSITE" >> /etc/hosts

# 1- Install apache and some dependencies
dnf -y install httpd httpd-tools mariadb-server mariadb vim wget tar git firewalld

# 2- enable and start services
systemctl enable --now httpd firewalld mariadb

# 3- open the port
firewall-cmd --permanent --add-service=http && sudo firewall-cmd --reload

# 7- Install php 8
dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

# 8- install remi
dnf -y install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-8.rpm

# 9- list php packages and update php
dnf -y module list php && sudo dnf -y module reset php

# 10- enable remi php
dnf -y module enable php:remi-8.2

# 11- install php and dependencies
dnf -y install php php-opcache php-gd php-curl php-mysqlnd php-pecl-zip php-intl

# 12- enable and start php
systemctl enable --now php-fpm

# 13- secure MariaDB server
mysql -u root -e "SET PASSWORD FOR root@localhost = PASSWORD('${DATABASE_PASSWD}'); FLUSH PRIVILEGES; \
    DELETE FROM mysql.user WHERE User=''; \
    DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1'    , '::1'); \
    DROP DATABASE test;DELETE FROM mysql.db WHERE Db='test' OR Db='test_%'; \
    FLUSH PRIVILEGES;"

# 17
mkdir -p /var/www/html/vhosts/${WEBSITE}

# 18
cd /var/www/html/vhosts/

# 19
wget http://wordpress.org/latest.tar.gz

# 20
tar -zxvf latest.tar.gz

# 21
cp -r /var/www/html/vhosts/wordpress/* /var/www/html/vhosts/${WEBSITE}

# 22
mkdir /var/www/html/vhosts/${WEBSITE}/wp-content/uploads

# 23
cd /var/www/html/vhosts/${WEBSITE}

# 24
cp wp-config-sample.php wp-config.php

# 25
sed -i "s/database_name_here/${SITE_DB}/" /var/www/html/vhosts/${WEBSITE}/wp-config.php

# 26 
sed -i "s/username_here/${DB_USER}/" /var/www/html/vhosts/${WEBSITE}/wp-config.php

# 27
sed -i "s/password_here/${USER_PASSWD}/" /var/www/html/vhosts/${WEBSITE}/wp-config.php

# 28 
chown -R apache:apache /var/www/html/vhosts/${WEBSITE}

# 29
mysql -u root -p${DATABASE_PASSWD} -e "CREATE DATABASE ${SITE_DB}; \
    CREATE USER '${DB_USER}'@'localhost' IDENTIFIED BY '${USER_PASSWD}'; \
    GRANT ALL PRIVILEGES ON ${SITE_DB}.* TO '${DB_USER}'@'localhost'; \
    FLUSH PRIVILEGES;"

# 33 add ServerName localhost on line 34 to fix an error
sed -i.bak '34 a ServerName localhost' /etc/httpd/conf/httpd.conf

# 34 Edit config file
cat >>/etc/httpd/conf.d/01-${WEBSITE}.conf<< EOF
<VirtualHost *:80>
    ServerName  servername.com
    ServerAlias www.servername.com
    DocumentRoot "/var/www/html/vhosts/servername.com"
    ErrorLog "/var/log/httpd/servername.com-error_log"
    CustomLog "/var/log/httpd/servername.com-access_log" common  
 <Directory "/var/www/vhosts/servername.com">
        Options Indexes FollowSymlinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF

# 35
sed -i "s/servername.com/$WEBSITE/g" /etc/httpd/conf.d/01-${WEBSITE}.conf

# 36 allows http to connect with other application
setsebool -P httpd_can_network_connect on

# 37 allow Apache to access file with different file content, this allows update through ftp
setsebool -P httpd_unified 1

# 38 Remove WordPress files
rm -rf /var/www/html/vhosts/wordpress /var/www/html/vhosts/latest.tar.gz

# 38 Restart httpd
systemctl restart httpd

# 39 configure epel repository
sudo dnf install -y epel-release
sudo dnf config-manager --set-enabled powertools

# 40 Install ImageMagick
sudo dnf install -y ImageMagick ImageMagick-devel

# 41 Install php imagick
sudo dnf install -y php php-devel php-pear php-pecl-imagick make

# 42 Update time
timedatectl set-timezone America/New_York

# 43 edit php.ini file
#sudo echo "extension=imagick.so" > /etc/php.d/20-imagick.ini

# 44 Secure trace
sed -i.bak '35 a TraceEnable off' /etc/httpd/conf/httpd.conf

# 45 Install rsyslog (will also activate /var/log/message and /var/log/secure)
#dnf -y install rsyslog
#sed -i '67i *.* @@192.168.0.90:514' /etc/rsyslog.conf
#systemctl enable --now rsyslog.service

# 46 Update the server
#dnf -y update
 
# 47 Restart the server
#systemctl reboot
